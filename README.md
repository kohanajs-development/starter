# Quick start

KohanaJS is a Node.js ORM framework using Fastify and Better SQLite3 as default web server and database.

###Installing
````
npm i kohanajs-start --save
````

Create folder structure, bootstrap.js and server files.
````
npx kohanajs-web
````

###Let's start the server.
````
npm i
npm start
````

Test your installation by opening http://localhost:8000