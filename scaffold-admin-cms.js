const path = require('node:path');
const { ncp } = require('ncp');
const { unlink } = require('fs/promises');
const note = require("./install-note");
const scaffoldAdmin = require("./scaffold-admin");

const init = async dir => {
  await scaffoldAdmin(dir);

  await new Promise((resolve, reject) => {
    ncp(path.normalize(`${__dirname}/default/admin-cms`), path.normalize(`${dir}`), err => {
      // callback
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('file copied');

      note.print_cms();
      resolve();
    });
  });
};

module.exports = async dir => {
  await init(dir);
};
