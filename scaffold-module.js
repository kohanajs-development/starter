const path = require('node:path');
const { ncp } = require('ncp');

const init = async dir => {
  await new Promise((resolve, reject) => {
    ncp(path.normalize(`${__dirname}/default/module`), path.normalize(`${dir}`), err => {
      // callback
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('file copied');
      resolve();
    });
  });
};

module.exports = async dir => {
  await init(dir);
};
