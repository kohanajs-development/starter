const mkdirp = require('mkdirp');
const { unlink } = require('fs/promises');
const path = require("node:path");

const scaffold = require('../scaffold');
const scaffoldWebform = require('../scaffold-webform');
const scaffoldAdmin = require('../scaffold-admin');
const scaffoldAdminCMS = require('../scaffold-admin-cms');

describe('scaffold test', () => {
  test('scaffold web', async () =>{
    const dir = path.normalize(`${__dirname}/test/web`);
    await mkdirp(dir);

    await scaffold(dir);

//    await unlink(dir);
  })

  test('scaffold webform', async () =>{
    const dir = path.normalize(`${__dirname}/test/webform`);
    await mkdirp(dir);

    await scaffoldWebform(dir);
  });

  test('scaffold admin', async () =>{
    const dir = path.normalize(`${__dirname}/test/admin`);
    await mkdirp(dir);

    await scaffoldAdmin(dir);
  });

  test('scaffold admin cms', async () =>{
    const dir = path.normalize(`${__dirname}/test/cms`);
    await mkdirp(dir);

    await scaffoldAdminCMS(dir);
  });
});