const {Controller, ControllerMixin} = require('@kohanajs/core-mvc');
const {ControllerMixinMultipartForm} = require('@kohanajs/mod-form');
const {ControllerMixinMime, ControllerMixinView, ControllerMixinDatabase, KohanaJS} = require('kohanajs');
const {ControllerMixinSession} = require('@kohanajs/mod-session');

class ControllerMixinStarter extends ControllerMixin{
  static async before(state){
    const client = state.get(ControllerMixin.CLIENT);
    const hostname = client.request.raw.hostname;
    const domain = hostname.split(':')[0];
    //push language, controller, action to layout
    const $_GET = state.get(ControllerMixinMultipartForm.GET_DATA);

    Object.assign(
      state.get(ControllerMixinView.LAYOUT).data,
      {
        domain : domain,
        controller : client.clientName,
        action: client.request.params.action,
        language : client.language,
        cookieConsent : client.request.cookies['allow-cookie'],
        cookies : JSON.stringify(client.request.cookies),
        utm_source:   encodeURIComponent($_GET['utm_source']   || ""),
        utm_medium:   encodeURIComponent($_GET['utm_medium']   || ""),
        utm_campaign: encodeURIComponent($_GET['utm_campaign'] || ""),
        utm_term:     encodeURIComponent($_GET['utm_term']     || ""),
        utm_content:  encodeURIComponent($_GET['utm_content']  || ""),
        logged_in : client.request.session.logged_in,
      }
    );
  }
}

class ControllerView extends Controller{
  static mixins = [ControllerMixinMime, ControllerMixinView, ControllerMixinMultipartForm, ControllerMixinDatabase, ControllerMixinSession, ControllerMixinStarter];

  constructor(request, controllerName="", layout='layout/default') {
    super(request);
    this.clientName = controllerName;

    this.state.set(ControllerMixinView.LAYOUT_FILE, layout)
    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', `${KohanaJS.config.setup.databaseFolder}/session.sqlite`);
  }

  async exit(code) {
    if(code === 500){
      this.setErrorTemplate(`templates/${this.language}/error`, {
        message: String(this.error).replace('Error:', ''),
        title: 'Error',
        language: this.language,
      });
    }
    await super.exit(code);
  }
}

module.exports = ControllerView;