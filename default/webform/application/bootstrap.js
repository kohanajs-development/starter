const { View } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, KohanaJS, ORM } = require('kohanajs');
const { LiquidView } = require('@kohanajs/mod-view-adapter-liquidjs');
const { ORMAdapterSQLite, DatabaseDriverBetterSQLite3 } = require("@kohanajs/mod-database-adapter-better-sqlite3");
require('@kohanajs/mod-form');
require("@kohanajs/mod-crypto");
require("@kohanajs/mod-session");

require('./requires');

ORM.defaultAdapter = ORMAdapterSQLite;
ControllerMixinDatabase.DEFAULT_DATABASE_DRIVER = DatabaseDriverBetterSQLite3;
View.DefaultViewClass = LiquidView;

module.exports = {
  modules: [],
};

KohanaJS.initConfig(new Map([
  ['setup', ''],
]));
