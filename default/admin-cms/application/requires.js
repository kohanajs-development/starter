require('@kohanajs/mod-form');
require("@kohanajs/mod-crypto");
require("@kohanajs/mod-session");
require("@kohanajs/mod-auth");
require("@kohanajs/mod-auth-adapter-password");
require("@kohanajs/mod-admin");
require("@kohanajs/mod-admin-view");
require("@kohanajs/mod-cms");