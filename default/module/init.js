const { KohanaJS } = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['example', require('./config/example')],
]));