const path = require('path');
const {KohanaJS} = require('kohanajs');
const {IdentifierPassword} = require("@kohanajs/mod-auth-adapter-password");

module.exports = {
  databasePath: path.normalize(KohanaJS.EXE_PATH + '/../database'),
  userDatabase: 'admin.sqlite',
  identifiers: [IdentifierPassword],
  destination: 'admin',
};
