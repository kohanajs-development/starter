require('dotenv').config()
const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = KohanaJS.ENV_TEST;
const Server = require('./Server');

(async () => {
  const s = new Server(parseInt(process.env.PORT ?? 8000) + 4);
  await s.setup();
  //  require("@kohanajs/mod-route-debug");
  await s.listen();
})();
